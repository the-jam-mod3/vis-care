import Jammed1 from './Jammed1.gif'
import Jammed2 from './Jammed2.gif'
import { NavLink } from 'react-router-dom';

function Jammed() {
    return (
        <div className="container-fluid text-center">
            <div className="card mb-3 p-3">
                <div className="row no-gutters">
                    <div className="card-body ">
                        <h1>YOU'VE BEEN JAMMED!</h1>
                        <div>
                            <img src={Jammed2} alt="loading..." />
                        </div>
                        <div>
                            <img src={Jammed1} alt="loading..." />
                        </div>
                        <div className="button-spacing p-2">
                            <NavLink to="/appointments" className="btn">Go Home</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Jammed;
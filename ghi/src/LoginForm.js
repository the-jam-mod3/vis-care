import useToken, { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";
import { login } from "./auth";
import { useNavigate } from 'react-router-dom';

const LoginForm = () => {
	const navigate = useNavigate();
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [error, setError] = useState("");
	const { setToken, baseUrl } = useAuthContext();
	const { token } = useToken();

	useEffect(() => {
		if ((token)) {
			navigate("/appointments");
		}
	}, [token, navigate])

	const handleSubmit = async (e) => {
		e.preventDefault();

		const form = e.currentTarget;
		try {
			const token = await login(baseUrl, username, password);
			setToken(token);
			navigate("/appointments");
		} catch (e) {
			if (e instanceof Error) {
				setError("Login Failed");
			}
			console.error(e);
		}
		form.reset();
	};

	const createAccount = async (e) => {
		navigate("/accounts/create")
	}

	return (
		<div className="card mb-3 p-3">
			<div className="row no-gutters">
				<div className="card-body">

					<h1>Login</h1>
					{error ? <div className="alert alert-danger" role="alert">{error}</div> : null}

					<form onSubmit={(e) => handleSubmit(e)}>
						<div className="form-floating mb-3">
							<input

								autoComplete="true"
								id="username"
								name="username"
								placeholder="Username"
								type="text"
								className="form-control"
								onChange={(e) => setUsername(e.target.value)}
							/>
							<label htmlFor="username">Username</label>
						</div>
						<div className="form-floating mb-3">
							<input

								autoComplete="true"
								id="password"
								name="password"
								placeholder="Password"
								type="password"
								className="form-control"
								onChange={(e) => setPassword(e.target.value)}
							/>
							<label htmlFor="password">Password</label>
						</div>
						<div>
							<input className="btn" type="submit" value="Login" />
							<button className="btn mx-2" type="submit" value="Create Account" onClick={createAccount} >Create Account</button>
						</div>
					</form>

				</div>
			</div>
		</div>
	);
};

export default LoginForm;

from fastapi import APIRouter, Depends, Response
from queries.appointments import (
    Appointment,
    AppointmentRepository,
    AppointmentOut
                )
from typing import List, Optional
from authenticator import authenticator

router = APIRouter()


@router.post("/appointments/", response_model=AppointmentOut)
def create_appointment(
    appointment: Appointment,
    repo: AppointmentRepository = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    return repo.create(appointment)


@router.get("/appointments/", response_model=List[AppointmentOut])
def get_appointments(
    repo: AppointmentRepository = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    return repo.get_all()


@router.get("/appointments/{appointment_id}",
            response_model=Optional[AppointmentOut])
def get_one(
    appointment_id: int,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AppointmentRepository = Depends(),
) -> AppointmentOut:
    appointment = repo.get_one(appointment_id)
    if appointment is None:
        response.status_code = 404
    return appointment


@router.put("/appointments/{appointment_id}", response_model=AppointmentOut)
def update_appointment(
    appointment_id: int,
    appointment: Appointment,
    repo: AppointmentRepository = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    return repo.update(appointment_id, appointment)


@router.delete("/appointments/{appointment_id}")
def delete_appointment(
    appointment_id: int,
    repo: AppointmentRepository = Depends(),
    account_data=Depends(authenticator.get_current_account_data)
):
    return repo.delete(appointment_id)

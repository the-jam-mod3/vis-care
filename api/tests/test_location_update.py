from main import app
from fastapi.testclient import TestClient
from queries.locations import LocationRepository, LocationOut
from queries.accounts import AdminOutWithPassword
from authenticator import authenticator


client = TestClient(app)


class UpdateLocationQuery:
    def update_location(self, id, location):
        return LocationOut(
            id=id,
            name=location.name,
            picture_url=location.picture_url)


class GetOneLocationQuery:
    def get_one(self, location_id):
        return LocationOut(id=location_id, name="TEST", picture_url="TEST_URL")


def fake_get_current_account_data():
    return AdminOutWithPassword(
        id=1,
        username="string",
        hashed_password="string")


def test_update_location():
    app.dependency_overrides[LocationRepository] = UpdateLocationQuery
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data

    location = {"name": "TEST", "picture_url": "TEST_URL"}

    response = client.put("/locations/1", json=location)
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == LocationOut(
        id=1,
        name="TEST",
        picture_url="TEST_URL")


def test_get_one_location():
    app.dependency_overrides[LocationRepository] = GetOneLocationQuery
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data

    response = client.get("/locations/1")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == LocationOut(
        id=1,
        name="TEST",
        picture_url="TEST_URL")

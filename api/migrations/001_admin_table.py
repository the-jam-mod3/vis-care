steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE admin (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(1000) NOT NULL UNIQUE,
            hashed_password VARCHAR(1000) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE admin;
        """
    ],
]
